
install:
	mkdir -p  /etc/fonts/conf.z/
	rm -f /etc/fonts/conf.z/*
	cp -f ./config/* /etc/fonts/conf.z/
	rm /etc/fonts/conf.d/* 
	cp -f ./config/* /etc/fonts/conf.d/
	fc-cache -sfv
.PHONY:  install 

